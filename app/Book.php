<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = ['id' ];

    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'book_id');
    }
}

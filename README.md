This is a book store api for getting books, authors, isbn and other query parameters required.

#Endpoints:

1. To create books: your-localhost/create_book

2. To create author: your-localhost/create_author

3. To get all books: your-localhost/get_books

4. To get a single book: your-localhost/get_book/id

5. To get an author: your-localhost/get_authot/id

6. To search book by title: your-localhost/search_books/keyword


7. To get author by id: your-localhost/search_author/id


